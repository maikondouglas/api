import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';

void main() {
  runApp(MaterialApp(
    home: API(),
  ));
}

class API extends StatefulWidget {
  @override
  _APIState createState() => _APIState();
}

class _APIState extends State<API> {
  TextEditingController controllerReal = TextEditingController();
  TextEditingController controllerDolar = TextEditingController();

  void _conveteRealDolar(String text) {}

  void _converteDolarReal(String text) {}

  Future<Map> consultaAPI() async {
    http.Response retorno = await http
        .get('http://api.hgbrasil.com/finance?format=json&key=b30189b2');

    return jsonDecode(retorno.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Conversor de moeda"),
        centerTitle: true,
        backgroundColor: Colors.blue[200],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
        child: Form(
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Digite o valor em REAL",
                  border: OutlineInputBorder(),
                ),
                controller: controllerDolar,
                onChanged: _conveteRealDolar,
              ),
              Divider(),
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Digite o valor em DOLAR",
                  border: OutlineInputBorder(),
                ),
                controller: controllerDolar,
                onChanged: _converteDolarReal,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
